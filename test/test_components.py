import sympy as sp
import sympy.physics.mechanics as me
import pytest

from suspycious import Model
import suspycious.components as scmp


def T_rigid_body():
    model = Model()
    body = model.add(scmp.RigidBody("A"))
    assert body.global_frame is model.frame


def T_add_fixed_point():
    model = Model()
    A = model.add(scmp.RigidBody("A"))
    p1 = A.add_fixed_point("p1", 1, 2, 3)
    pos_from_com = A.p1.pos_from(A.com)
    assert p1.vel(A.frame) == 0
    assert p1.acc(A.frame) == 0
    assert pos_from_com == A.frame.x + 2 * A.frame.y + 3 * A.frame.z
    assert p1.frame == A.frame


def T_six_dof_spring():
    kx, ky, kz = sp.symbols("k_x, k_y, k_z", positive=True)
    Kx, Ky, Kz = sp.symbols("kappa_x, kappa_y, kappa_z", positive=True)
    model = Model()
    A = model.add(scmp.RigidBody("A"))
    A.body.apply_force(-kx * A.x_cm * A.frame.x)
    A.body.apply_force(-ky * A.y_cm * A.frame.y)
    A.body.apply_force(-kz * A.z_cm * A.frame.z)
    A.body.apply_torque(-Kx * A.tx_cm * A.frame.x)
    A.body.apply_torque(-Ky * A.ty_cm * A.frame.y)
    A.body.apply_torque(-Kz * A.tz_cm * A.frame.z)
    print(A.body.loads)
    # ss = model.extract_statespace(True)
    # print(ss.outputs)


def T_apply_gravity():
    g = sp.symbols("g", positive=True)
    model = Model()
    A = model.add(scmp.RigidBody("A"))
    B = model.add(scmp.RigidBody("B"))
    Fa = -A.M * g * model.frame.z
    Fb = -B.M * g * model.frame.z
    A.body.clear_loads()
    B.body.clear_loads()
    A.apply_gravity()
    assert len(A.body.loads) == 1
    assert len(B.body.loads) == 0
    assert A.body.loads[0][1] == Fa
    model.apply_gravity()
    assert len(A.body.loads) == 1
    assert len(B.body.loads) == 1
    assert A.body.loads[0][1] == Fa
    assert B.body.loads[0][1] == Fb
    assert A.body.loads[0][0] == A.com
    assert B.body.loads[0][0] == B.com


def T_protect_set_op_point():
    """
    Check that setting attributes for symbols and dynamic symbols sets their
    operating points and that attempting to set any other attributes is disallowed
    """
    body = scmp.RigidBody("A")
    assert isinstance(body.M, sp.Symbol)
    assert body.M not in body.op_point
    body.M = 3
    assert body.op_point[body.M] == 3
    assert isinstance(body.M, sp.Symbol)
    assert me.find_dynamicsymbols(body.x) == {body.x}
    body.x = 6
    assert me.find_dynamicsymbols(body.x) == {body.x}
    assert body.op_point[body.x] == 6
    with pytest.raises(AttributeError):
        body.a = 3
