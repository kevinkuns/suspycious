import pytest

from suspycious import Model
import suspycious.components as suscmp


def T_model_add():
    model = Model()
    body = model.add(suscmp.RigidBody("A"))
    assert isinstance(body, suscmp.RigidBody)
    assert body.name == "A"
    assert body is model.A
    assert body is model.components["A"]
    assert body.model is model
    body2 = suscmp.RigidBody("B")
    assert body2.model is None
    model.add(body2)
    assert body2.model is model


def T_add_component_same_name():
    with pytest.raises(AttributeError):
        model = Model()
        model.add(suscmp.RigidBody("A"))
        model.add(suscmp.RigidBody("A"))


def T_add_component_to_existing_model():
    with pytest.raises(AssertionError):
        model1 = Model()
        model2 = Model()
        body = model1.add(suscmp.RigidBody("A"))
        model2.add(body)
