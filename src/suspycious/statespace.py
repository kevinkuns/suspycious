import numpy as np
import sympy as sp

from wield.control import MIMO
from wield.bunch import Bunch
from wield.utilities.file_io import save


def convert_key(key):
    var_map = {
        "theta": "t",
        "delta": "d",
        "epsilon": "dt",
        "omega": "w",
    }
    comp, sym = key.name.split(".")
    var = sym.split("_")
    assert len(var) in [1, 2]
    if len(var) == 2:
        var = [var_map.get(var[0], var[0]) + var[1]]
    return f"{comp}.{var[0]}"


class StateSpace:
    def __init__(self, A, B, C=None, D=None, E=None, inputs=None, outputs=None):
        self._A = A
        self._B = B
        self._C = C
        self._D = D
        self._E = E
        self._inputs = inputs
        self._outputs = outputs

    @property
    def A(self):
        return self._A

    @property
    def B(self):
        return self._B

    @property
    def C(self):
        return self._C

    @property
    def D(self):
        return self._D

    @property
    def E(self):
        return self._E

    @property
    def Nstates(self):
        return self.A.shape[-1]

    @property
    def Ninputs(self):
        return self.B.shape[-1]

    @property
    def Noutputs(self):
        return self.C.shape[-2]

    @property
    def inputs(self):
        return self._inputs

    @property
    def outputs(self):
        return self._outputs

    def _to_numpy(self):
        # FIXME: check for unevaluated symbols
        sp2np = lambda mat: np.array(mat).astype(float)
        return Bunch(
            A=sp2np(self.A),
            B=sp2np(self.B),
            C=sp2np(self.C),
            D=sp2np(self.D),
            E=sp2np(self.E),
            inputs={convert_key(k): v for k, v in self.inputs.items()},
            outputs={convert_key(k): v for k, v in self.outputs.items()},
        )

    def to_file(self, fname):
        save(fname, self._to_numpy())

    def to_wield(self):
        ss = self._to_numpy()
        return MIMO.MIMOStateSpace(
            ss.A, ss.B, ss.C, ss.D, ss.E, inputs=ss.inputs, outputs=ss.outputs,
        )

class KanesMethodStateSpace(StateSpace):
    def __init__(self, kanel, op_point, include_velocities=False):
        self._kanel = kanel

        self._sol = kanel.linearize(op_point=op_point)
        self._input_vector = self._sol[-1]
        self._state_vector = sp.zeros(len(kanel.q) + len(kanel.u), 1)
        self._state_vector[:len(kanel.q), 0] = kanel.q
        self._state_vector[len(kanel.q):, 0] = kanel.u
        inputs = {key: idx for idx, key in enumerate(kanel.r)}
        outputs = {key: idx for idx, key in enumerate(kanel.q)}
        if include_velocities:
            outputs.update({
                key: idx + len(kanel.q) for idx, key in enumerate(kanel.u)
            })
        super().__init__(
            A=self._sol[1],
            B=self._sol[2],
            C=self._build_C_matrix(outputs),
            D=sp.zeros(len(outputs), len(inputs)),
            E=self._sol[0],
            inputs=inputs,
            outputs=outputs,
        )

    @property
    def kanel(self):
        return self._kanel

    def _build_C_matrix(self, outputs):
        Nstates = self._sol[1].shape[-1]
        Noutputs = len(outputs)
        C = sp.zeros(Noutputs, Nstates)
        for idx in outputs.values():
            C[idx, idx] = 1
        return C
        
