import sympy as sp
import sympy.physics.mechanics as mech
from sympy.physics.mechanics import System, Force
from .statespace import KanesMethodStateSpace
from .components import RigidBody
from sympy.physics.mechanics import System


class Model:
    def __init__(self):
        sys = System()
        self.__system = sys
        self.__reference_frame = sys.frame
        self.__origin = sys.fixed_point
        self.origin.set_vel(self.frame, 0)
        self.__components = dict()

    @property
    def frame(self):
        return self.__reference_frame

    # def frame(self):
    #     return sys.frame

    @property
    def origin(self):
        return self.__origin

    @property
    def system(self):
        return self.__system

    def add(self, component):
        if hasattr(self, component.name):
            raise AttributeError(
                f"A component named {component.name} alread exists"
            )
        component._set_model(self)
        component._on_add()
        setattr(self, component.name, component)
        self.__components[component.name] = component
        return component

    @property
    def components(self):
        return self.__components

    def connect(self, *args, **kwargs):
        pass

    def apply_gravity(self):
        for component in self.components.values():
            component.apply_gravity()

    def find_operating_point(self):
        for component in self.components.values():
            component.find_operating_point()

    @property
    def op_point(self):
        op_point = dict()
        for component in self.components.values():
            op_point.update(component.op_point)
        return op_point

    def op_subs(self, expr, subs=dict()):
        subs.update(self.op_point)
        return mech.msubs(expr, subs)

    def extract_statespace(self, include_velocities=False):

        for component in self.components.values():
            if not isinstance(component, RigidBody):
                continue
            for q in component.q_ind:
                self.system.add_coordinates(q)
            for u in component.u_ind:
                self.system.add_speeds(u)
            for kde in component.kd_eqs:
                self.system.add_kdes(kde)

        self.system.validate_system()
        self.system.form_eoms()
        kanel = self.system.eom_method.to_linearizer()

        return KanesMethodStateSpace(
            kanel=kanel, op_point=self.op_point
        )
