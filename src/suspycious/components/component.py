from abc import ABC, abstractmethod

import sympy as sp
import sympy.physics.mechanics as mech


class Component(ABC):
    def __new__(cls, *args, **kwargs):
        instance = super().__new__(cls)
        instance.__dict__["__protected"] = False
        instance.__dict__["_symbols"] = []
        instance.__dict__["_dynamicsymbols"] = []
        return instance

    def __init__(self, name):
        self.__name = name
        self.__model = None
        self._op_point = dict()
        self.__gravity_applied = False
        self._protect()

    @property
    def name(self):
        return self.__name

    @property
    def symbols(self):
        return {key: self.get(key) for key in self._symbols}

    @property
    def dynamicsymbols(self):
        return {key: self.get(key) for key in self._dynamicsymbols}

    def get(self, key):
        return self.__dict__.get(key)

    def _add_symbol(self, key, sym, level=None, **assumptions):
        if self.__dict__["__protected"]:
            raise AttributeError(
                "Cannot add a symbol when the component is protected"
            )
        if level is None:
            symbol = sp.symbols(f"{self.name}.{sym}", **assumptions)
            self._symbols.append(key)
        elif isinstance(level, int) and level >= 0:
            symbol = mech.dynamicsymbols(
                f"{self.name}.{sym}", level=level, **assumptions,
            )
            self._dynamicsymbols.append(key)
        else:
            raise ValueError("level must be None or a non-negative integer")
        # setattr(self, key, symbol)
        self.__dict__[key] = symbol
        return symbol

    def _protect(self):
        self.__dict__["__protected"] = True

    def _unprotect(self):
        self.__dict__["__protected"] = False

    def __setattr__(self, name, value):
        """
        This hackily
        1) prevents symbols from being overwritten
        2) setting the attribute of a symbol sets its operating point
        3) no other attributes can be added or changed if the component is
        protected
        """
        if name in (self._symbols + self._dynamicsymbols):
            self.op_point[self.get(name)] = value
        else:
            if self.__dict__["__protected"] and not name.startswith("_"):
                raise AttributeError(
                    "Cannot set attribute when the component is protected"
                )
            super().__setattr__(name, value)

    @abstractmethod
    def _on_add(self):
        pass

    def apply_gravity(self):
        if not self.__gravity_applied:
            self._apply_gravity()
            self.__gravity_applied = True

    @abstractmethod
    def _apply_gravity(self):
        pass

    @abstractmethod
    def find_operating_point(self):
        pass

    @property
    def model(self):
        return self.__model

    def _set_model(self, model):
        assert self.model is None
        self.__model = model

    @property
    def global_frame(self):
        return self.model.frame

    @property
    def global_origin(self):
        return self.model.origin

    @property
    def op_point(self):
        return self._op_point

    def op_subs(self, expr, subs=dict()):
        subs.update(self.op_point)
        return mech.msubs(expr, subs)
