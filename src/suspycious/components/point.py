from sympy.physics.mechanics import Point


class FixedPoint(Point):
    def __new__(cls, name, frame):
        instance = super().__new__(cls)
        instance.__frame = frame
        return instance

    def __init__(self, name, frame):
        super().__init__(name)

    @property
    def frame(self):
        return self.__frame
