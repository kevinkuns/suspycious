import sympy as sp
import sympy.physics.mechanics as mech

from .body import RigidBody


class Beam(RigidBody):
    pass


class BladeSpring(Beam):
    pass
