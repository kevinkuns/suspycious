import sympy as sp
import sympy.physics.mechanics as mech

from .component import Component
from .point import FixedPoint
from .. import constants as const


class RigidBody(Component):
    def __init__(self, name, zero_initial_velocities=True):
        super().__init__(name)
        self._unprotect()
        self.__reference_frame = mech.ReferenceFrame(name)
        self._points = dict()
        # body properties
        self._add_symbol("M", "M", positive=True)
        self._add_symbol("Ixx", "I_{xx}", positive=True)
        self._add_symbol("Iyy", "I_{yy}", positive=True)
        self._add_symbol("Izz", "I_{zz}", positive=True)
        self._add_symbol("Ixy", "I_{xy}", real=True)
        self._add_symbol("Iyz", "I_{yz}", real=True)
        self._add_symbol("Izx", "I_{zx}", real=True)
        # generalized coordinates
        self._add_symbol("x", "x", level=0, real=True)
        self._add_symbol("y", "y", level=0, real=True)
        self._add_symbol("z", "z", level=0, real=True)
        self._add_symbol("tx", "theta_x", level=0, real=True)
        self._add_symbol("ty", "theta_y", level=0, real=True)
        self._add_symbol("tz", "theta_z", level=0, real=True)
        # generalized velocities
        self._add_symbol("ux", "u_x", level=0, real=True)
        self._add_symbol("uy", "u_y", level=0, real=True)
        self._add_symbol("uz", "u_z", level=0, real=True)
        self._add_symbol("wx", "omega_x", level=0, real=True)
        self._add_symbol("wy", "omega_y", level=0, real=True)
        self._add_symbol("wz", "omega_z", level=0, real=True)
        # time derivatives
        self._add_symbol("x_d", "x", level=1, real=True)
        self._add_symbol("y_d", "y", level=1, real=True)
        self._add_symbol("z_d", "z", level=1, real=True)
        self._add_symbol("tx_d", "theta_x", level=1, real=True)
        self._add_symbol("ty_d", "theta_y", level=1, real=True)
        self._add_symbol("tz_d", "theta_z", level=1, real=True)
        # forcing coordinates
        self._add_symbol("dx", "delta_x", level=0, real=True)
        self._add_symbol("dy", "delta_y", level=0, real=True)
        self._add_symbol("dz", "delta_z", level=0, real=True)
        self._add_symbol("dtx", "epsilon_x", level=0, real=True)
        self._add_symbol("dty", "epsilon_y", level=0, real=True)
        self._add_symbol("dtz", "epsilon_z", level=0, real=True)
        # forces and torques
        self._add_symbol("Fx", "F_x", level=0, real=True)
        self._add_symbol("Fy", "F_y", level=0, real=True)
        self._add_symbol("Fz", "F_z", level=0, real=True)
        self._add_symbol("Tx", "T_x", level=0, real=True)
        self._add_symbol("Ty", "T_y", level=0, real=True)
        self._add_symbol("Tz", "T_z", level=0, real=True)

        self.x_cm = self.x + self.dx
        self.y_cm = self.y + self.dy
        self.z_cm = self.z + self.dz
        self.tx_cm = self.tx + self.dtx
        self.ty_cm = self.ty + self.dty
        self.tz_cm = self.tz + self.dtz
        zero_syms = [
            self.dx, self.dy, self.dz, self.dtx, self.dty, self.dtz,
            self.Fx, self.Fy, self.Fz, self.Tx, self.Ty, self.Tz,
        ]
        if zero_initial_velocities:
            zero_syms.extend([
                self.ux, self.uy, self.uz, self.wx, self.wy, self.wz,
            ])
        for sym in zero_syms:
            self.op_point[sym] = 0

        self._com = mech.Point(f"{name}.com")
        self._I = mech.inertia(
            self.frame, self.Ixx, self.Iyy, self.Izz,
            self.Ixy, self.Iyz, self.Izx,
        )
        self.__body = mech.Body(
            name=f"{name}_bdy",
            masscenter=self.com,
            mass=self.M,
            central_inertia=self.mom_inertia,
            frame=self.frame,
        )

        self.com.set_vel(frame=self.frame, value=0)
        self.body.apply_force(self.Fx * self.frame.x)
        self.body.apply_force(self.Fy * self.frame.y)
        self.body.apply_force(self.Fz * self.frame.z)
        self.body.apply_torque(self.Tx * self.frame.x)
        self.body.apply_torque(self.Ty * self.frame.y)
        self.body.apply_torque(self.Tz * self.frame.z)
        self._protect()

    @property
    def frame(self):
        return self.__reference_frame

    @property
    def body(self):
        return self.__body

    @property
    def com(self):
        return self._com

    @property
    def mom_inertia(self):
        return self._I

    @property
    def points(self):
        return self._points

    def add_fixed_point(self, name, dx, dy, dz):
        if hasattr(self, name):
            raise AttributeError(f"{self.name} already has an attribute {name}")
        point = FixedPoint(f"{self.name}.{name}", frame=self.frame)
        pos = dx * self.frame.x + dy * self.frame.y + dz * self.frame.z
        point.set_pos(otherpoint=self.com, value=pos)
        point.set_vel(frame=self.frame, value=0)
        point.v2pt_theory(
            otherpoint=self.com,
            outframe=self.global_frame,
            fixedframe=self.frame,
        )
        self._points[name] = point
        self._unprotect()
        setattr(self, name, point)
        self._protect()
        return point

    def _on_add(self):
        ang_vel = self.wx * self.frame.x + self.wy * self.frame.y + self.wz * self.frame.z
        vel = self.ux * self.frame.x + self.uy * self.frame.y + self.uz * self.frame.z
        pos = (
            self.x_cm * self.global_frame.x
            + self.y_cm * self.global_frame.y
            + self.z_cm * self.global_frame.z
        )
        self.frame.orient_body_fixed(
            parent=self.global_frame,
            angles=(self.tx_cm, self.ty_cm, self.tz_cm),
            rotation_order="xyz",
        )
        self.frame.set_ang_vel(otherframe=self.global_frame, value=ang_vel)
        self.com.set_pos(otherpoint=self.global_origin, value=pos)
        self.com.set_vel(frame=self.global_frame, value=vel)
        self.model.system.add_bodies(self.body)

    def set_global_position(self, x_cm, y_cm, z_cm):
        self.op_point[self.x] = x_cm
        self.op_point[self.y] = y_cm
        self.op_point[self.z] = z_cm

    def set_global_orientation(self, tx_cm, ty_cm, tz_cm):
        self.op_point[self.tx] = tx_cm
        self.op_point[self.ty] = ty_cm
        self.op_point[self.tz] = tz_cm

    def find_operating_point(self):
        # the operating point of a RigidBody should always be specified
        # in some other way and should not need to be calculated
        pass

    def _apply_gravity(self):
        self.body.apply_force(
            force=-self.M * const.g * self.global_frame.z,
            point=self.com,
        )

    @property
    def q_ind(self):
        return [self.x, self.y, self.z, self.tx, self.ty, self.tz]

    @property
    def u_ind(self):
        return [self.ux, self.uy, self.uz, self.wx, self.wy, self.wz]

    @property
    def kd_eqs(self):
        return [
            self.ux - self.x_d, self.uy - self.y_d, self.uz - self.z_d,
            self.wx - self.tx_d, self.wy - self.ty_d, self.wz - self.tz_d,
        ]


class Block(RigidBody):
    pass


class Cylinder(RigidBody):
    pass
