from sympy.physics.mechanics import Point, RigidBody
from sympy import zeros, symbols, Matrix
import suspycious.components as scmp
import sympy as sp
import numpy as np


def get_tension_dirs(body1, body2, points_1='top',
                     points_2='top', suspension_body=None):

    body1_points = list(body1.points.values())
    body2_points = list(body2.points.values())

    if points_1 == 'top':
        body1_points = body1_points[:4]
    else:
        body1_points = body1_points[4:]
    if points_2 == 'top':
        body2_points = body2_points[:4]
    else:
        body2_points = body2_points[4:]

    print(body1_points)
    print(body2_points)
    S = suspension_body
    ten_dirs = [i.pos_from(j).express(S.global_frame).normalize() for i, j in list(zip(body1_points, body2_points))]

    return ten_dirs


def give_deltas(body1, body2, suspension_body,
                model, points_1='top', points_2='top'):

    body1_points = list(body1.points.values())
    body2_points = list(body2.points.values())

    if points_1 == 'top':
        body1_points = body1_points[:4]
    else:
        body1_points = body1_points[4:]
    if points_2 == 'top':
        body2_points = body2_points[:4]
    else:
        body2_points = body2_points[4:]

    points_ = list(zip(body1_points, body2_points))

    op_point = model.op_point
    sb = suspension_body
    print(points_)
    deltas_ = [i.pos_from(j).express(sb.global_frame).magnitude() - i.pos_from(j).express(sb.global_frame).subs(op_point).magnitude() for i, j in points_]
    return deltas_


def add_attachment_points(name, body, position='top', *args, **kwargs):
    points = [Point('{}{}'.format(name, j)) for j in range(1, 9)]
    points_ = [str(i) for i in points]  # these are the points
    matrix_attachment = Matrix([(1, -1, 1), (1, 1, 1), (-1, -1, 1), (-1, 1, 1),
                                (1, -1, -1), (1, 1, -1), (-1, -1, -1), (-1, 1, -1)])

    if position == 'top':
        for i in range(4):
            body._unprotect()
            pt = points_[i]
            dx_pt = body._add_symbol(f"{pt}_x", f"{pt}_x", level=0, real=True)
            dy_pt = body._add_symbol(f"{pt}_y", f"{pt}_y", level=0, real=True)
            dz_pt = body._add_symbol(f"{pt}_z", f"{pt}_z", level=0, real=True)
            attachment_points = [dx_pt, dy_pt, dz_pt]
            matrix_col_1 = matrix_attachment[i:i+1, 0]*attachment_points[0]
            matrix_col_2 = matrix_attachment[i:i+1, 1]*attachment_points[1]
            matrix_col_3 = matrix_attachment[i:i+1, 2]*attachment_points[2]
            a = [matrix_col_1[0, 0], matrix_col_2[0, 0], matrix_col_3[0, 0]]
            print(a[0], a[1], a[2])
            body.add_fixed_point(points_[i], a[0], a[1], a[2])

    else:
        for i in range(4, 8):
            body._unprotect()
            pt = points_[i]
            dx_pt = body._add_symbol(f"{pt}_x", f"{pt}_x", level=0, real=True)
            dy_pt = body._add_symbol(f"{pt}_y", f"{pt}_y", level=0, real=True)
            dz_pt = body._add_symbol(f"{pt}_z", f"{pt}_z", level=0, real=True)
            attachment_points = [dx_pt, dy_pt, dz_pt]

            matrix_col_1 = matrix_attachment[i:i+1, 0]*attachment_points[0]
            matrix_col_2 = matrix_attachment[i:i+1, 1]*attachment_points[1]
            matrix_col_3 = matrix_attachment[i:i+1, 2]*attachment_points[2]
            a = [matrix_col_1[0, 0], matrix_col_2[0, 0], matrix_col_3[0, 0]]
            print(a[0], a[1], a[2])
            body.add_fixed_point(points_[i], a[0], a[1], a[2])

    body._protect()
